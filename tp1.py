import cv2
from matplotlib import pyplot as plt
import numpy as np
import sys

# Acquiring image and template
imagename = sys.argv[1]
img = cv2.imread(imagename, 0)
template = cv2.imread('blackRec.png', 0)
w, h = template.shape[::-1] # Template dimensions


# Binary Threshold
ret, img = cv2.threshold(img,127,255,cv2.THRESH_BINARY)

# Template Matching
res = cv2.matchTemplate(img,template,cv2.TM_CCOEFF_NORMED)
threshold = 0.72
loc = np.where( res >= threshold)
j = (-21,-21)
mylist = zip(*loc[::-1])
mylist.sort()
mylist2 = []
mylist3 = []

def getKey(item):
    return item[1]

print mylist
for i in mylist:
    if abs(i[0] - j[0]) > 20 or abs(i[1] - j[1]) > 20:
        mylist2.append(i)
        j = i

mylist2 = sorted(mylist2, key=getKey)

j = (-21,-21)
for i in mylist2:
    if abs(i[0] - j[0]) > 20 or abs(i[1] - j[1]) > 20:
        mylist3.append(i)
        j = i

print(mylist2)
print(mylist3)

for pt in mylist3:
    #print (pt, (pt[0] + w, pt[1] + h))
    cv2.rectangle(img, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)

x_list = []
y_list = []

for pt in mylist3[:12]:
    x_list.append(pt[0])
for pt in mylist3[12:]:
    y_list.append(pt[1])

x_list.sort()
del x_list[0]
del x_list[5]

a = 0
hists = []

for i in x_list:
    for j in y_list:
        cv2.rectangle(img, (i,j), (i + w, j + h), (0,0,255), 2)
        cv2.putText(img, "%d" % a, (i - 5, j), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0), 2)
        window = img[j:j + h, i:i + w]
        hists.append(cv2.calcHist([window], [0], None, [2], [0,256]))
        a += 1


questions = []

for i in xrange(15):
    question = []
    for j in xrange(5):
        question.append(i + j*15)
    questions.append(question)

for i in xrange(15):
    question = []
    for j in xrange(5):
        question.append(75 + i + j*15)
    questions.append(question)

print questions

answer_file = open(imagename + '.ans', 'w')
answer_model = ['A','B','C','D','E','Branco','Nulo']

for k in xrange(30):
    question = questions[k]
    x = 5
    for i in xrange(5):
        rate = hists[question[i]][0]/(hists[question[i]][0] + hists[question[i]][1])
        if rate > 0.55:
            if x == 5:
                print rate
                x = i
            else:
                x = 6
                break
    answer_file.write(('%d: ' % (k+1)) + answer_model[x] + '\n')

cv2.imwrite('res.png',img)


plt.imshow(img, cmap="gray")
plt.show()
